from common import init, set_light
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
import math
from random import shuffle, uniform

WINDOW_SIZE = 800

WHITE = [1.0, 1.0, 1.0, 1.0]
CYAN = [0.0, 1.0, 1.0, 1.0]
BLUE = [0.0, 0.0, 1.0, 1.0]
MAGENTA = [1.0, 0.0, 1.0, 1.0]

COLOR = CYAN

CIRCLE = None
SQUARE = None

ALL_POINTS = None
CURRENT = 0

TRANSFORM_STEP = 1


def get_circle_points(radius, step):
    points = []
    angle = 0.0
    while angle < (math.pi * 2):
        x = math.cos(angle) * radius
        y = math.sin(angle) * radius
        points.append([x, y])
        angle += step
    return points


def get_square_points(points_number, radius):
    points = []
    one_line = points_number // 4
    step = (2.0 * radius) / one_line

    for i in range(one_line):
        x = -radius
        y = -radius + i * step
        points.append([x, y])

    for i in range(one_line):
        x = radius
        y = -radius + i * step
        points.append([x, y])

    for i in range(one_line):
        y = -radius
        x = -radius + i * step
        points.append([x, y])

    for i in range(one_line):
        y = radius
        x = -radius + i * step
        points.append([x, y])

    rest = points_number - len(points)
    for i in range(rest):
        point = points[len(points) - 1]
        points.append([point[0], point[1]])
    return points


def get_arrays(first, second, steps=10):
    result = []
    for i in range(steps + 1):
        array = []
        for j in range(len(first)):
            x1 = first[j][0]
            x2 = second[j][0]
            x = x1 + i * ((x2 - x1) / steps)

            y1 = first[j][1]
            y2 = second[j][1]
            y = y1 + i * ((y2 - y1) / steps)

            array.append([x, y])
        result.append(array)
    return result


def draw():
    print('drawing')
    step = 0.01
    points = 300
    cover_points = 100
    start_position = -1.5

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()
    glBegin(GL_POINTS)

    # glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, CYAN)
    # glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, CYAN)

    for point in ALL_POINTS[CURRENT]:
        x = point[0]
        y = point[1]
        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, COLOR)
        for i in range(points):
            glVertex3f(x, y, start_position + i * step)
        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, COLOR)
        for i in range(cover_points):
            glVertex3f(uniform(0.0, x), uniform(0.0, y), start_position)
            glVertex3f(uniform(0.0, x), uniform(0.0, y), start_position + points * step)

    glEnd()

    # glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, MAGENTA)
    # glutSolidSphere(1, 40, 40)

    glPopMatrix()
    glutSwapBuffers()


def affect(key, x, y):
    if key == GLUT_KEY_RIGHT:
        move_to_next_state()
        print('key pressed, current = {}'.format(CURRENT))


def move_to_next_state():
    global CURRENT, TRANSFORM_STEP
    # global COLOR

    CURRENT += TRANSFORM_STEP
    # COLOR = CYAN if CURRENT % 2 == 0 else BLUE
    draw()

    if CURRENT == len(ALL_POINTS) - 1:
        TRANSFORM_STEP = -1
    if CURRENT == 0:
        TRANSFORM_STEP = 1


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE)
    glutInitWindowPosition(10, 10)
    glutCreateWindow('Morphing')

    init()
    # set_light([GL_LIGHT0],
    #           [[10., 10., 10., 0.]],
    #           [WHITE])

    glutDisplayFunc(draw)
    glutSpecialFunc(affect)

    glMatrixMode(GL_PROJECTION)
    gluPerspective(15., 1., 1., 100.)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(10, 10, 10,
              0, 0, 0,
              0, 0, 5)

    glPushMatrix()
    glutMainLoop()


if __name__ == '__main__':
    r = 1.0

    CIRCLE = get_circle_points(radius=r, step=0.01)
    SQUARE = get_square_points(points_number=len(CIRCLE), radius=r)

    shuffle(CIRCLE)
    shuffle(SQUARE)

    ALL_POINTS = get_arrays(CIRCLE, SQUARE, steps=8)
    CURRENT = 0

    print('circle points number = {0}'.format(len(CIRCLE)))
    print('square points number = {0}'.format(len(SQUARE)))
    main()
