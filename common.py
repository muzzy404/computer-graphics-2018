from OpenGL.GL import *

WINDOW_SIZE = 600


def init():
    glClearColor(0, 0, 0, 0)
    glShadeModel(GL_SMOOTH)
    glEnable(GL_CULL_FACE)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_LIGHTING)


def set_light(lights, positions, colors):
    for light, position, color in zip(lights, positions, colors):
        glLightfv(light, GL_POSITION, position)
        glLightfv(light, GL_DIFFUSE, color)
        glEnable(light)
