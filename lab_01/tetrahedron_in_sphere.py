from common import init, WINDOW_SIZE
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys

SPHERE_SIZE = 1.5
SOLID_TETRAHEDRON = True
SPHERE = True

x_rot = 0.0
y_rot = 0.0
z_rot = 0.0


def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()

    glRotatef(x_rot, 1.0, 0.0, 0.0)
    glRotatef(y_rot, 0.0, 1.0, 0.0)
    glRotatef(z_rot, 0.0, 0.0, 1.0)
    # sphere
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, [1.0, 1.0, 1.0, 1.0])
    if SPHERE:
        glutWireSphere(SPHERE_SIZE, 40, 40)  # radius, slices, stacks
    # tetrahedron
    glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, [1.0, 1.0, 1.0, 1.0])
    glutSolidTetrahedron() if SOLID_TETRAHEDRON else glutWireTetrahedron()

    glPopMatrix()
    glutSwapBuffers()


def rotate(key, x, y):
    global x_rot, y_rot, z_rot
    global SOLID_TETRAHEDRON, SPHERE
    step = 2.0
    if key == GLUT_KEY_UP:
        x_rot += step
    if key == GLUT_KEY_DOWN:
        x_rot -= step
    if key == GLUT_KEY_LEFT:
        y_rot += step
    if key == GLUT_KEY_RIGHT:
        y_rot -= step
    if key == GLUT_KEY_END:
        z_rot += step
    if key == GLUT_KEY_HOME:
        z_rot -= step
    if key == GLUT_KEY_F1:
        SOLID_TETRAHEDRON = not SOLID_TETRAHEDRON
    if key == GLUT_KEY_F2:
        SPHERE = not SPHERE
    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE)
    glutCreateWindow('Tetrahedron in sphere')
    init()

    glutDisplayFunc(display)
    glutSpecialFunc(rotate)

    glMatrixMode(GL_PROJECTION)
    gluPerspective(15, 1, 1, 100)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(10, 10, 10,
              0, 0, 0,
              0, 0, 5)
    glPushMatrix()
    glutMainLoop()


if __name__ == '__main__':
    main()
