from common import init, set_light, WINDOW_SIZE
from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys

CUBE_SIZE = 2.0
CONE_BASE = CUBE_SIZE/2
CONE_HEIGHT = CUBE_SIZE
SOLID_CUBE = True

WHITE = [1.0, 1.0, 1.0, 1.0]
RED = [1.0, 0.0, 0.0, 1.0]

dx = 0.0
dy = 0.0
dz = -CUBE_SIZE/2  # to draw base of cone in the bottom of cube


def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()
    # cube
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, WHITE)
    glutWireCube(CUBE_SIZE) if SOLID_CUBE else glutSolidCube(CUBE_SIZE)
    # cone
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, RED)
    glTranslate(dx, dy, dz)
    glutSolidCone(CONE_BASE, CONE_HEIGHT, 50, 50)
    glPopMatrix()
    glutSwapBuffers()


def affect(key, x, y):
    step = 0.05
    global dx, dy, dz
    global CUBE_SIZE, SOLID_CUBE
    if key == GLUT_KEY_DOWN:
        dy += step
    if key == GLUT_KEY_UP:
        dy -= step
    if key == GLUT_KEY_LEFT:
        dx += step
    if key == GLUT_KEY_RIGHT:
        dx -= step
    if key == GLUT_KEY_PAGE_UP:
        dz += step
    if key == GLUT_KEY_PAGE_DOWN:
        dz -= step
    if key == GLUT_KEY_INSERT:
        SOLID_CUBE = not SOLID_CUBE
    if key == GLUT_KEY_F1:
        CUBE_SIZE += 0.1
    if key == GLUT_KEY_F2 and CUBE_SIZE > 0:
        CUBE_SIZE -= 0.1
    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE)
    glutCreateWindow('Cube and cone')

    init()
    set_light([GL_LIGHT0, GL_LIGHT1, GL_LIGHT2],
              [[20., 10., 0., 0.], [-10., 0., 10., 0.], [0., -10., -20., 0.]],
              [WHITE, WHITE, WHITE])

    glutDisplayFunc(display)
    glutSpecialFunc(affect)

    glMatrixMode(GL_PROJECTION)
    gluPerspective(15., 1., 1., 100.)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(10, 10, 10,
              0, 0, 0,
              0, 0, 5)
    glPushMatrix()
    glutMainLoop()


if __name__ == '__main__':
    main()
