from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from PIL import Image

WINDOW_SIZE = 840

V = [[+1, +1, +1],
     [+1, +1, -1],
     [+1, -1, -1],
     [-1, -1, -1],
     [-1, -1, +1],
     [-1, +1, +1],
     [+1, -1, +1],
     [-1, +1, -1]]

PLANES = [
    (V[2], V[3], V[7], V[1]),  # 1
    (V[6], V[2], V[1], V[0]),  # 2
    (V[4], V[6], V[0], V[5]),  # 3
    (V[3], V[4], V[5], V[7]),  # 4
    (V[6], V[4], V[3], V[2]),  # 5
    (V[5], V[0], V[1], V[7]),  # 6
]

COORDS = ([0, 0], [1, 0], [1, 1], [0, 1])

dx = 0.0
dy = 0.0
dz = 0.0
steps = [0.1, 0.1, 0.05]


def init_gl(width, height):
    glClearColor(0.0, 0.0, 0.0, 0.0)
    glClearDepth(1.0)
    glDepthFunc(GL_LESS)
    glEnable(GL_DEPTH_TEST)
    glShadeModel(GL_SMOOTH)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(45.0, float(width) / float(height), 0.1, 100.0)
    glMatrixMode(GL_MODELVIEW)

    # initialize texture mapping
    glEnable(GL_TEXTURE_2D)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)


def draw_gl_scene():
    global dx, dy, dz

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)

    glLoadIdentity()
    glTranslatef(0.0, 0.0, -6.0)

    glRotatef(dx, 1.0, 0.0, 0.0)
    glRotatef(dy, 0.0, 1.0, 0.0)
    glRotatef(dz, 0.0, 0.0, 1.0)

    # Draw Cube (multiple quads)
    glBegin(GL_QUADS)
    for plane in PLANES:
        for vertex, coord in zip(plane, COORDS):
            glTexCoord2f(coord[0], coord[1])
            glVertex3f(vertex[0], vertex[1], vertex[2])
    glEnd()
    glutSwapBuffers()

    dx = round(dx + steps[0], 2)
    dy = round(dx + steps[1], 2)

    for i, d in zip(range(2), [dx, dy, dz]):
        if d < -200 or d > 200:
            steps[i] *= -1
    print(dx, dy, dz)


def load_image():
    image = Image.open("politeh.jpg")

    ix = image.size[0]
    iy = image.size[1]
    image = image.tobytes("raw", "RGBX", 0, -1)

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1)
    glTexImage2D(GL_TEXTURE_2D, 0, 3, ix, iy, 0, GL_RGBA, GL_UNSIGNED_BYTE, image)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
    glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_ALPHA | GLUT_DEPTH)
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE)
    glutInitWindowPosition(10, 10)

    glutCreateWindow('OpenGL Python Textured Cube')

    glutDisplayFunc(draw_gl_scene)
    glutIdleFunc(draw_gl_scene)
    init_gl(WINDOW_SIZE, WINDOW_SIZE)
    load_image()
    glutMainLoop()


if __name__ == "__main__":
    main()
