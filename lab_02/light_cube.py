from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
from common import WINDOW_SIZE, init, set_light

CUBE_SIZE = 2.0

COLOR_R = True
COLOR_G = True
COLOR_B = True

light_x = 80.0
light_y = 20.0
light_z = 30.0


def display():
    r = 1 if COLOR_R else 0
    g = 1 if COLOR_G else 0
    b = 1 if COLOR_B else 0

    print(light_x, light_y, light_z)
    set_light([GL_LIGHT0],
              [[light_x, light_y, light_z, 0]],
              [[r, g, b, 0.0]])

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()

    # cube
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, [1.0, 1.0, 1.0, 0.0])
    glutSolidCube(CUBE_SIZE)

    glPopMatrix()
    glutSwapBuffers()


def rotate(key, x, y):
    global COLOR_R, COLOR_G, COLOR_B
    global light_x, light_y, light_z

    step = 2.0

    if key == GLUT_KEY_F1:
        COLOR_R = not COLOR_R
    if key == GLUT_KEY_F2:
        COLOR_G = not COLOR_G
    if key == GLUT_KEY_F3:
        COLOR_B = not COLOR_B

    if key == GLUT_KEY_PAGE_UP:
        light_z += step
    if key == GLUT_KEY_PAGE_DOWN:
        light_z -= step
    if key == GLUT_KEY_LEFT:
        light_x += step
    if key == GLUT_KEY_RIGHT:
        light_x -= step
    if key == GLUT_KEY_DOWN:
        light_y += step
    if key == GLUT_KEY_UP:
        light_y -= step

    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE)
    glutCreateWindow('Light cube')

    init()

    glutDisplayFunc(display)
    glutSpecialFunc(rotate)

    glMatrixMode(GL_PROJECTION)
    gluPerspective(15, 1, 1, 100)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(10, 10, 10,
              0, 0, 0,
              0, 0, 5)
    glPushMatrix()
    glutMainLoop()


if __name__ == '__main__':
    main()
