from OpenGL.GLUT import *
from OpenGL.GLU import *
from OpenGL.GL import *
import sys
from common import WINDOW_SIZE, init, set_light

SPHERE_SIZE = 1.5
LIGHT_COLOR = [1.0, 1.0, 1.0, 1.0]

BRIGHTNESS = 0.50

z_rot = 65.0
light_z = 30.0


def display():
    print(z_rot, light_z)
    set_light([GL_LIGHT0],
              [[-15, 30, light_z, 0]],
              [LIGHT_COLOR])

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glPushMatrix()

    glRotatef(z_rot, 0.0, 0.0, 1.0)

    # tetrahedron
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, [0.0, 0.0, 1.0, 1.0])
    glutSolidTetrahedron()

    glEnable(GL_BLEND)
    glDepthMask(GL_FALSE)
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    # sphere
    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, [1.0, 0.0, 0.0, BRIGHTNESS])
    glutSolidSphere(SPHERE_SIZE, 40, 40)

    glDepthMask(GL_TRUE)
    glDisable(GL_BLEND)

    glPopMatrix()
    glutSwapBuffers()


def rotate(key, x, y):
    global BRIGHTNESS
    global z_rot, light_z

    step = 0.05
    size_step = 2.0
    light_step = 2.0

    if key == GLUT_KEY_DOWN:
        BRIGHTNESS -= step if BRIGHTNESS > 0.0 else 0
    if key == GLUT_KEY_UP:
        BRIGHTNESS += step if BRIGHTNESS < 1.0 else 0

    if key == GLUT_KEY_RIGHT:
        z_rot += size_step
    if key == GLUT_KEY_LEFT:
        z_rot -= size_step

    if key == GLUT_KEY_PAGE_UP:
        light_z += light_step
    if key == GLUT_KEY_PAGE_DOWN:
        light_z -= light_step

    glutPostRedisplay()


def main():
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH)
    glutInitWindowSize(WINDOW_SIZE, WINDOW_SIZE)
    glutCreateWindow('Tetrahedron in sphere')

    init()

    glutDisplayFunc(display)
    glutSpecialFunc(rotate)

    glMatrixMode(GL_PROJECTION)
    gluPerspective(15, 1, 1, 100)
    glMatrixMode(GL_MODELVIEW)
    gluLookAt(10, 10, 10,
              0, 0, 0,
              0, 0, 5)
    glPushMatrix()
    glutMainLoop()


if __name__ == '__main__':
    main()
